import axios from 'axios'

const $axios = axios.create({
  baseURL: 'https://b507qiqddb.execute-api.eu-central-1.amazonaws.com/torque',
  headers: {
    'Accept': 'application/json'
  }
});

export default $axios